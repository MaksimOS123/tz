from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User

from Work.forms import SignInForm
from Work.models import UserProfile
from Work.forms import UserProfileEdit
import Work.functions as f_m


# Create your views here.

def index_page(request):
    context = f_m.get_base_context(request)
    context['title'] = "Main page"
    return render(request, 'index.html', context)


def user_page(request, user_id):
    context = f_m.get_base_context(request)
    user = User.objects.get(id=user_id)
    context['title'] = user
    context['first_name'] = user.first_name
    context['last_name'] = user.last_name
    context['email'] = user.email
    context['username'] = user
    context['photo'] = False
    if UserProfile.objects.filter(user=user).exists():
        context['date'] = str(UserProfile.objects.get(user=user).birth_date)
        if UserProfile.objects.get(user=user).photo:
            context['photo'] = UserProfile.objects.get(user=user).photo
    else:
        context['date'] = "unknown"

    return render(request, 'simple_page.html', context)


@login_required
def profile(request):
    context = f_m.get_base_context(request)
    context['title'] = "Profile"
    user = request.user
    context['first_name'] = user.first_name
    context['last_name'] = user.last_name
    context['email'] = user.email
    context['username'] = user
    context['photo'] = False
    if UserProfile.objects.filter(user=user).exists():
        context['date'] = str(UserProfile.objects.get(user=user).birth_date)
        if UserProfile.objects.get(user=user).photo:
            context['photo'] = UserProfile.objects.get(user=user).photo
    else:
        context['date'] = "unknown"
    return render(request, 'profile.html', context)


@login_required
def profile_edit(request):
    if not UserProfile.objects.filter(user=request.user).exists():
        UserProfile(user=request.user).save()
    context = f_m.get_base_context(request)
    context['title'] = 'Edit profile'
    context['first_name'] = request.user.first_name
    context['last_name'] = request.user.last_name
    context['email'] = request.user.email
    if UserProfile.objects.filter(user=request.user).exists():
        context['date'] = str(UserProfile.objects.get(user=request.user).birth_date)
    else:
        context['date'] = "YYYY-MM-DD"

    user = User.objects.filter(username=request.user)[0]
    if request.method == 'POST':
        if request.POST.get('email', False):
            user.email = request.POST.get('email')
            user.save()
        if request.POST.get('first_name', False) and request.POST.get('last_name', False):
            user.first_name, user.last_name = request.POST.get('first_name'), request.POST.get('last_name')
            user.save()
        if request.POST.get('date', False):
            my_user_profile = UserProfile.objects.get(user=request.user)
            my_user_profile.birth_date = request.POST.get('date')
            my_user_profile.save()
            context['success'] = 'Изменения сохранены'
        if request.POST.get('old_pswd', False):
            if user.check_password(request.POST.get('old_pswd')):
                if request.POST.get('new_pswd') == request.POST.get('conf_pswd'):
                    user.set_password(request.POST.get('new_pswd'))
                    user.save()
                    context['success'] = 'Пароль успешно изменен'
                else:
                    context['error'] = 'Новые пароли не совпадают'
            else:
                context['error'] = 'Старый пароль неверный'

    this_user = UserProfile.objects.get(user=request.user)
    if request.method == 'POST':
        profile_form = UserProfileEdit(instance=this_user, data=request.POST, files=request.FILES)
        if profile_form.is_valid():
            profile_form.save()
    else:
        profile_form = UserProfileEdit(instance=this_user)

    context['profile_form'] = profile_form

    return render(request, 'profile_edit.html', context)


def sign_up(request):
    context = f_m.get_base_context(request)
    context['title'] = 'Регистрация'
    context['errors'] = []

    if request.method == 'POST':
        f = SignInForm(request.POST)

        if f.is_valid():
            u_n = f.data['user_name']
            u_fn = f.data['user_first_name']
            u_ln = f.data['user_last_name']
            u_em = f.data['user_email']
            u_pw = f.data['password']
            u_pw_c = f.data['password_conf']

            if not f_m.is_existing_user(User.objects.all(), u_n):
                if u_pw == u_pw_c:
                    new_user = User.objects.create_user(username=u_n, first_name=u_fn, last_name=u_ln, email=u_em,
                                                        password=u_pw)
                    new_user.save()
                    return HttpResponseRedirect('/login/')
                else:
                    context['errors'].append("Введенные пароли не совпадают")
            else:
                context['errors'].append("Пользователь с таким логином уже существует")

            context['form'] = f
        else:
            context['form'] = f
    else:
        f = SignInForm()
        context['form'] = f

    return render(request, 'registration/register.html', context)
