from django import forms
from Work.models import UserProfile

class SignInForm(forms.Form):
    user_name = forms.CharField(label="Login:", widget=forms.TextInput(attrs={'class': 'float-right'}))
    user_first_name = forms.CharField(label="First name:", widget=forms.TextInput(attrs={'class': 'float-right'}))
    user_last_name = forms.CharField(label="Last name:", widget=forms.TextInput(attrs={'class': 'float-right'}))
    user_email = forms.EmailField(label="Email:", widget=forms.TextInput(attrs={'class': 'float-right'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'float-right'}), label="Пароль:")
    password_conf = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'float-right'}),
                                    label="Подтвердите пароль:")

class UserProfileEdit(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('photo',)
