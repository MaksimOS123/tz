# Generated by Django 3.0.7 on 2020-07-11 19:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Work', '0003_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='photo',
            field=models.ImageField(default=0, upload_to='images/'),
            preserve_default=False,
        ),
    ]
