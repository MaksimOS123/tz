def get_base_context(request):
    menu = []
    if request.user.is_authenticated:
        menu.append({'link': '/profile/', 'text': 'Профиль'})
        menu.append({'link': '/logout/', 'text': 'Выйти'})
    else:
        menu.append({'link': '/login/', 'text': 'Войти'})
        menu.append({'link': '/user/add/', 'text': 'Регистрация'})

    return {
        'menu': menu,
    }

def is_existing_user(users_list, username):
    for user in users_list:
        if user.username == username:
            return True

    return False
